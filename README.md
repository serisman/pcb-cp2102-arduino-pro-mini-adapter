# CP2012 USB to TTL to Arduino Pro Mini Adapter v1.0 #

## Schematic ##

![Schematic.PNG](https://bytebucket.org/serisman/pcb-cp2102-arduino-pro-mini-adapter/raw/master/output/Schematic.PNG)

## Design Rules ##

* Clearance: 7 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 7 mil
* Zone Min Width: 7 mil
* Zone Thermal Antipad Clearance: 7 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.3" x 0.6" (7.65 mm x 15.27 mm)
* $0.30 each ($0.90 for 3)
* [https://oshpark.com/shared_projects/yRoYwJm6](https://oshpark.com/shared_projects/yRoYwJm6)

### PCB Front ###

![PCB-Front.png](https://bytebucket.org/serisman/pcb-cp2102-arduino-pro-mini-adapter/raw/master/output/PCB-Front.png)

### PCB Back ###

![PCB-Back.png](https://bytebucket.org/serisman/pcb-cp2102-arduino-pro-mini-adapter/raw/master/output/PCB-Back.png)

### CP2102 USB to TTL ###

NOTE: If you want to use the auto-reset capabilities, you will need to cut the trace going to the RST pin and connect it to the DTR header pin (red dots/line in the picture below)

![cp2102-usb-ttl.jpg](https://bytebucket.org/serisman/pcb-cp2102-arduino-pro-mini-adapter/raw/master/doc/cp2102-usb-ttl.jpg)

### Arduino Pro Mini ###

![arduino-pro-mini.jpg](https://bytebucket.org/serisman/pcb-cp2102-arduino-pro-mini-adapter/raw/master/doc/arduino-pro-mini.jpg)
EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:circuit-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "15 aug 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_6 K1
U 1 1 53CB4783
P 4950 3950
F 0 "K1" V 4900 3950 50  0000 C CNN
F 1 "CONN_6" V 5000 3950 40  0000 C CNN
F 2 "" H 4950 3950 60  0000 C CNN
F 3 "" H 4950 3950 60  0000 C CNN
	1    4950 3950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_6 K2
U 1 1 53ED502D
P 6300 3950
F 0 "K2" V 6250 3950 50  0000 C CNN
F 1 "CONN_6" V 6350 3950 40  0000 C CNN
F 2 "" H 6300 3950 60  0000 C CNN
F 3 "" H 6300 3950 60  0000 C CNN
	1    6300 3950
	1    0    0    1   
$EndComp
Text Label 5300 3700 0    60   ~ 0
DTR
Text Label 5300 3800 0    60   ~ 0
3V3
Text Label 5300 3900 0    60   ~ 0
5V
Text Label 5300 4000 0    60   ~ 0
TXD
Text Label 5300 4100 0    60   ~ 0
RXD
Text Label 5300 4200 0    60   ~ 0
GND
Text Label 5950 3700 2    60   ~ 0
GND
Text Label 5950 3800 2    60   ~ 0
GND
Text Label 5950 3900 2    60   ~ 0
VCC
Text Label 5950 4000 2    60   ~ 0
RXD
Text Label 5950 4100 2    60   ~ 0
TXD
Text Label 5950 4200 2    60   ~ 0
DTR
$Comp
L JUMPER3 JP1
U 1 1 53ED51D1
P 5600 3350
F 0 "JP1" H 5650 3250 40  0000 L CNN
F 1 "JUMPER3" H 5600 3450 40  0000 C CNN
F 2 "~" H 5600 3350 60  0000 C CNN
F 3 "~" H 5600 3350 60  0000 C CNN
	1    5600 3350
	-1   0    0    1   
$EndComp
Text Label 5600 3250 0    60   ~ 0
VCC
Text Label 5850 3350 0    60   ~ 0
3V3
Text Label 5350 3350 2    60   ~ 0
5V
$EndSCHEMATC
